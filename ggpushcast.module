<?php

/**
 * @file
 * Contains ggpushcast.module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_form_BASE_FORM_ID_alter() for node form.
 */
function ggpushcast_form_node_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  // If a user doesn't have the permission to send notifications, then exit.
  $user = \Drupal::currentUser();
  if ($user->hasPermission('send notifications through ggpushcast') == FALSE) {
    return;
  }

  /* @var \Drupal\node\NodeInterface $node */
  $node = $form_state->getFormObject()->getEntity();
  $type = $node->getType();
  $config = \Drupal::config('ggpushcast.settings');
  // Is a user still didn't store push notifications settings then return.
  if ($config->isNew()) {
    return;
  }
  // Node types that was enabled for notification sending on the settings page.
  $types_enabled = $config->get('node_types');
  // Check if notification sending is enabled for this node type.
  if (empty($types_enabled) or !array_key_exists($type, $types_enabled)) {
    return;
  }

  // The checkbox to define if a content must be pushed to 'G&G Pushcast'.
  $form['actions']['push_content'] = [
    '#type' => 'checkbox',
    '#title' => t('Push this content to G&G Pushcast'),
  ];

  // If this is node Edit form and a node is published then uncheck
  // 'push_content' checkbox.
  $is_checked = FALSE;
  if (!$node->isNew() and $node->isPublished()) {
    $is_checked = FALSE;
  }
  // Check if the 'always_notify' option is set.
  elseif ($types_enabled[$type]['always_notify']) {
    $is_checked = TRUE;
  }

  $form['actions']['push_content']['#default_value'] = $is_checked;

  // Add the custom submit handler.
  array_unshift($form['actions']['submit']['#submit'], 'ggpushcast_node_form_submit');
}

/**
 * Custom node form submit handler.
 *
 * A node publishing can be delayed, for instance, it can be done later with
 * the 'Rules' module.
 * Therefore we cannot implement the notification sending in the submit handler.
 * Instead of, we implement it in the 'hook_ENTITY_TYPE_update()' and in the
 * 'hook_ENTITY_TYPE_insert()' (for a new node).
 * Here we just store nid into the configuration if the 'push_content' checkbox
 * is checked.
 * This will be checked in the 'hook_ENTITY_TYPE_update()' to make
 * a decision should we send a notification for a node or not.
 */
function ggpushcast_node_form_submit(array $form, FormStateInterface $form_state) {
  $node = $form_state->getFormObject()->getEntity();

  $config = \Drupal::service('config.factory')->getEditable('ggpushcast.settings');
  $nids = $config->get('nids') ?? [];

  // If the 'push_content' checkbox is checked.
  if ($form_state->getValue('push_content')) {
    // Add nid to the configuration.
    if (!in_array($node->id(), $nids)) {
      $nids[] = $node->id();
    }
  }
  else {
    // Exclude nid from the configuration.
    $key = array_search($node->id(), $nids);
    unset($nids[$key]);
  }

  $config->set('nids', $nids)->save();

  // We can't get id of a new node here, but we can get it
  // in the hook_ENTITY_TYPE_insert().
  // Also, store the value of 'push_content' checkbox in the custom property
  // to get it in 'the hook_ENTITY_TYPE_insert()'.
  if ($node->isNew()) {
    $node->push_content = $form_state->getValue('push_content');
  }
}

/**
 * Implements hook_ENTITY_TYPE_insert() for node entities.
 *
 * Provide notifications sending for new nodes if the 'push_content' checkbox
 * is checked. If a new node status is 'Published' then send a notification to
 * the 'G&G Pushcast' service.
 * But if the status is 'Unpublished' (delayed publishing), then store id of a
 * node in the configuration. In this case, a notification will be send in
 * the 'hook_ENTITY_TYPE_update()' when a node will be published.
 */
function ggpushcast_node_insert(NodeInterface $node) {
  if ($node->push_content) {
    // Store a node id to be able to send a notification in case of delayed
    // publishing.
    $config = \Drupal::service('config.factory')->getEditable('ggpushcast.settings');
    $nids = $config->get('nids') ?? [];
    $nids[] = $node->id();
    $config->set('nids', $nids);
    $config->save();

    if ($node->isPublished()) {
      
      ggpushcast_update_node_alias($node, 'insert');
      // Send data to the 'G&G Pushcast' service.
      \Drupal::service('ggpushcast.notifications_manager')->push($node);
    }
    return;
  }
}

/**
 * Implements hook_ENTITY_TYPE_update() for node entities.
 *
 * Send data to the 'G&G Pushcast' service if the 'push_content' checkbox was
 * checked in a node Edit form.
 */
function ggpushcast_node_update(NodeInterface $node) {
  if ($node->isPublished() == FALSE) {
    return;
  }

  $type = $node->getType();
  $config = \Drupal::config('ggpushcast.settings');
  // Node types that was enabled for notification sending on the settings page.
  $types_enabled = $config->get('node_types');

  // If a node publishing was delayed, that it's possible the situation
  // when a user will change the settings to disable notification sending for
  // this node type. In this case, to prevent a notification sending, we need
  // to ensure that a notification sending is still enabled for this node type.
  if (empty($types_enabled) or !array_key_exists($type, $types_enabled)) {
    return;
  }
  // If the 'push_content' checkbox was checked for a node then push data to
  // the 'G&G Pushcast' service.
  $nids = $config->get('nids');

  if ($nids) {
    if (in_array($node->id(), $nids)) {

      ggpushcast_update_node_alias($node, 'update');
      // Send data to the 'G&G Pushcast' service.
      \Drupal::service('ggpushcast.notifications_manager')->push($node);
    }
  }
}

/**
 * If the "Pathauto" module is installed, then update a node alias.
 *
 * If we do not do this, then the node alias may not be generated yet. 
 * In this case a node's path without an alias will be sent to the push
 * notification service. Also, can be the situation that will be send old, 
 * but not updated alias.
 */
function ggpushcast_update_node_alias(NodeInterface $node, $op) {
  $module_handler = \Drupal::service('module_handler');
  
  // Check if the "Pathauto" module is installed.
  if ($module_handler->moduleExists('pathauto')) {
    \Drupal::service('pathauto.generator')->updateEntityAlias($node, $op);  
  }
}

/**
 * Implements hook_page_attachments().
 */
function ggpushcast_page_attachments(array &$attachments) {
  $attachments['#attached']['library'][] = 'ggpushcast/ggpushcast.notifications_dialog';
}

/**
 * Implements hook_help().
 */
function ggpushcast_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the ggpushcast module.
    case 'help.page.ggpushcast':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= t('Provides integration with the <a href=":url">@name</a> push notifications service.', [
        ':url' => 'https://ggpushcast.com',
        '@name' => 'G&G Pushcast',
      ]);
      return $output;
  }
}
