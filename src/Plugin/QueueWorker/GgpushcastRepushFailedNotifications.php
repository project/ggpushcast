<?php

namespace Drupal\ggpushcast\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\ggpushcast\GgpushcastNotificationsManagerInterface;
use Drupal\Core\State\StateInterface;

/**
 * Plugin implementation of ggpushcast_repush_failed_notifications queueworker.
 *
 * @QueueWorker (
 *   id = "ggpushcast_repush_failed_notifications",
 *   title = @Translation("Retry to push notifications, that failed to be pushed."),
 *   cron = {"time" = 30}
 * )
 */
class GgpushcastRepushFailedNotifications extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\ggpushcast\GgpushcastNotificationsManagerInterface definition.
   *
   * @var \Drupal\ggpushcast\GgpushcastNotificationsManagerInterface
   */
  protected $notificationsManager;

  /**
   * State service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a new class instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\ggpushcast\GgpushcastNotificationsManagerInterface $notifications_manager
   *   Ggpushcast notifications manager service.
   * @param \Drupal\Core\State\StateInterface $state
   *   State service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, GgpushcastNotificationsManagerInterface $notifications_manager, StateInterface $state) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->notificationsManager = $notifications_manager;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('ggpushcast.notifications_manager'),
      $container->get('state'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($nid) {

    $node = $this->entityTypeManager->getStorage('node')->load($nid);

    // We try to repush a notification only once on Crone runs (further a user
    // can only repush manually). Therefore remove a data from the Cron queue
    // to prevent repeating of resending on Cron runs.
    $cron_queue_items = $this->state->get('ggpushcast_cron_queue_items');
    unset($cron_queue_items[$nid]);
    $this->state->set('ggpushcast_cron_queue_items', $cron_queue_items);

    // Check if a node still exists.
    if (is_null($node)) {
      return;
    }
    // Check if a node still published.
    if ($node->isPublished() == FALSE) {
      return;
    }

    // Check if notification sending is still enabled for this node type.
    $config = $this->configFactory->getEditable('ggpushcast.settings');
    // Node types that was enabled for notification sending on settings page.
    $types_enabled = $config->get('node_types');
    $type = $node->getType();

    if (empty($types_enabled) or !array_key_exists($type, $types_enabled)) {
      return;
    }

    $this->notificationsManager->repush($node);
  }

}
