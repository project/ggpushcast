<?php

namespace Drupal\ggpushcast;

use GuzzleHttp\ClientInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\State\StateInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Database\Connection;

/**
 * Notifications pushing and the log creation.
 */
class GgpushcastNotificationsManager implements GgpushcastNotificationsManagerInterface {

  /**
   * Endpoint of the service.
   */
  const ENDPOINT = 'https://ggpushcast.com/push';

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The storage handler class for files.
   *
   * @var \Drupal\file\FileStorage
   */
  protected $fileStorage;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The queue for resending failed notifications when Cron runs.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * State service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a new GgpushcastDataSender object.
   */
  public function __construct(ClientInterface $http_client, EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, RequestStack $request_stack, QueueFactory $queue_factory, Connection $connection, StateInterface $state) {
    $this->httpClient = $http_client;
    $this->fileStorage = $entity_type_manager->getStorage('file');
    $this->configFactory = $config_factory;
    $this->request = $request_stack->getCurrentRequest();
    $this->queue = $queue_factory->get('ggpushcast_repush_failed_notifications');
    $this->connection = $connection;
    $this->state = $state;
  }

  /**
   * Create a notification payload for a node to sending to a REST endpoint.
   *
   * @param \Drupal\node\NodeInterface $node
   *   A node that was published and for which we need to send a notification.
   *
   * @return array
   *   An associative array containing a payload for sending to a REST endpoint.
   */
  private function createNotificationPayload(NodeInterface $node) {
    $config = $this->configFactory->get('ggpushcast.settings');
    $type = $node->getType();
    // Get notification settings for the current node type.
    $settings = $config->get('node_types')[$type];

    /* ------------ Get elements for a payload ----------------------------- */

    /* ------------ 'api_key' ---------------------------------------------- */
    $api_key = $config->get('api_key');

    /* ------------ 'domain' ----------------------------------------------- */
    $domain = $this->request->getHost();

    /* ------------ 'body' ------------------------------------------------- */
    $body = $title = $node->getTitle();

    /* ------------ 'image' ------------------------------------------------ */
    // The 'image' maybe used as the 'icon', if a user selected this option.
    $image = '';

    switch ($settings['image_source']) {

      case 'body':
        // Get the url of a first image from a node's body.
        $subject = $node->get('body')->value;
        $result = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $subject, $matches);
        if ($result) {
          $host = $this->request->getSchemeAndHttpHost();
          $image = $host . $matches[1][0];
        }
        break;

      case 'field':
        $image_field = $settings['image_field'];
        $image_entity = $node->get($image_field)->entity;
        if ($image_entity) {
          $image = file_create_url($image_entity->getFileUri());
        }
        break;
    }

    /* ------------ 'icon' ------------------------------------------------- */

    // We will use a logo and a favicon of a front theme.
    // Get the name of a front theme (non admin theme).
    $front_theme_name = $this->configFactory->get('system.theme')->get('default');
    $icon = '';

    switch ($config->get('icon_source')) {

      case 'site_logo':
        $host = $this->request->getSchemeAndHttpHost();
        $icon = $host . theme_get_setting('logo.url', $front_theme_name);
        break;

      case 'content':
        $icon = $image;
        break;

      case 'ggpushcast.com':
        $icon = '';
        break;

      case 'uploaded_image':
        $fid = $config->get('icon_file');
        if ($fid) {
          $file = $this->fileStorage->load($fid);
          $icon = file_create_url($file->getFileUri());
        }
        break;
    }

    /* ------------ 'badge' ------------------------------------------------ */

    $badge = '';

    switch ($config->get('badge_source')) {

      case 'favicon':
        $host = $this->request->getSchemeAndHttpHost();
        $badge = $host . theme_get_setting('favicon.url', $front_theme_name);
        break;

      case 'uploaded_image':
        $badge = '';
        $fid = $config->get('badge_file');
        if ($fid) {
          $file = $this->fileStorage->load($fid);
          $badge = file_create_url($file->getFileUri());
        }
        break;
    }

    /* ------------ 'title' ------------------------------------------------ */

    $website_name = $this->configFactory->get('system.site')->get('name');

    switch ($settings['title_pattern']) {

      case 'site_name_only':
        $title = $website_name;
        break;

      case 'site_name_and_taxonomy':
        $title = $website_name;
        $taxonomy_field = $settings['taxonomy_field'];
        $terms = $node->get($taxonomy_field)->referencedEntities();
        if (!empty($terms)) {
          // Get a name of a first term.
          $term_name = $terms[0]->getName();
          $title = $website_name . ' - ' . $term_name;
        }
        break;

      case 'taxonomy_and_site_name':
        $title = $website_name;
        $taxonomy_field = $settings['taxonomy_field'];
        $terms = $node->get($taxonomy_field)->referencedEntities();
        if (!empty($terms)) {
          $term_name = $terms[0]->getName();
          $title = $term_name . ' - ' . $website_name;
        }
        break;
    }

    /* ------------ 'url' -------------------------------------------------- */

    $url = $node->toUrl('canonical', ['absolute' => TRUE])->toString();

    /* --------------------------------------------------------------------- */

    $payload = [
      'api_key' => $api_key,
      'domain' => $domain,
      'message' => [
        'body' => $body,
        'icon' => $icon,
        'badge' => $badge,
        'image' => $image,
        'title' => $title,
        'data' => [
          'url' => $url,
        ],
      ],
    ];

    return $payload;
  }

  /**
   * Send a notification data to the 'G&G Pushcast' service.
   *
   * @param \Drupal\node\NodeInterface $node
   *   A node that was published and for which we need to send a notification.
   *
   * @return bool
   *   TRUE if a data was sent successfully.
   */
  public function push(NodeInterface $node) {

    $config = $this->configFactory->get('ggpushcast.settings');
    $logging_enabled = $config->get('logging_enabled');

    $nid = $node->id();
    $payload = $this->createNotificationPayload($node);

    $response = $this->httpClient->request('POST', self::ENDPOINT, [
      'json' => $payload,
      'http_errors' => FALSE,
    ]);

    $status_code = $response->getStatusCode();

    $config = $this->configFactory->getEditable('ggpushcast.settings');

    // If sending successful.
    if ($status_code == 200 && $logging_enabled) {

      // Exclude log from the failed logs list.
      $failed_logs = $this->state->get('ggpushcast_failed_logs');
      unset($failed_logs[$nid]);
      $this->state->set('ggpushcast_failed_logs', $failed_logs);

      // Store log in the success logs list.
      $success_logs = $this->state->get('ggpushcast_success_logs');
      $success_logs[$nid] = [
        'title' => $node->getTitle(),
        'date' => time(),
      ];

      $this->state->set('ggpushcast_success_logs', $success_logs);
    }
    // If sending failed.
    else {

      // Add a data to a Cron queue to resend when Cron is run.
      $cron_queue_item_id = $this->queue->createItem($nid);
      // Also, store an id of Cron queue item to be able to
      // remove an item from a Cron queue in case of manually notification
      // repushing will be successful. It used in the 'repush()' method of the
      // 'GgpushcastRepushFailedNotificationController'.
      $cron_queue_items = $this->state->get('ggpushcast_cron_queue_items');
      $cron_queue_items[$nid] = $cron_queue_item_id;
      $this->state->set('ggpushcast_cron_queue_items', $cron_queue_items);

      if ($logging_enabled) {

        $failed_logs = $this->state->get('ggpushcast_failed_logs');
        // We don't store a node's title because when we will display log of
        // failed notificationa or when repushing we use a last actual title.
        $failed_logs[$nid] = [
          'date' => time(),
          'status_code' => $status_code,
        ];

        $this->state->set('ggpushcast_failed_logs', $failed_logs);
      }
    }

    return $status_code;
  }

  /**
   * Resend a notification data to the 'G&G Pushcast' service.
   *
   * The method is called when a user try to resend a data that wasn't sent due
   * to a failure.
   * To do this a user click on the 'Push' button in the log message table.
   * Also, this method could be called from the queue worker when Cron runs.
   * The difference from the 'push()' method is that here we don't create the
   * log message if the pushing was failed.
   *
   * @param \Drupal\node\NodeInterface $node
   *   A node that was published and for which we need to send a notification.
   *
   * @return int
   *   The code of a response:
   *   200 - SUCCESS;
   *   400 - MESSAGE NOT FOUND OR WRONG;
   *   401 - AUTHENTICATION_FAILURE.
   */
  public function repush(NodeInterface $node) {

    $nid = $node->id();
    $payload = $this->createNotificationPayload($node);

    $response = $this->httpClient->request('POST', self::ENDPOINT, [
      'json' => $payload,
      'http_errors' => FALSE,
    ]);

    $status_code = $response->getStatusCode();

    // If a data was sent successfully.
    if ($status_code == 200 && $logging_enabled) {

      // Exclude log from the failed logs list.
      $failed_logs = $this->state->get('ggpushcast_failed_logs');
      unset($failed_logs[$nid]);
      $this->state->set('ggpushcast_failed_logs', $failed_logs);

      // Store log in the success logs list.
      $success_logs = $this->state->get('ggpushcast_success_logs');
      $success_logs[$nid] = [
        'title' => $node->getTitle(),
        'date' => time(),
      ];

      $this->state->set('ggpushcast_success_logs', $success_logs);
    }

    return $status_code;
  }

}
