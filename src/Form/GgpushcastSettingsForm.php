<?php

namespace Drupal\ggpushcast\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;

/**
 * Settings form.
 */
class GgpushcastSettingsForm extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Entity\EntityFieldManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The storage handler class for files.
   *
   * @var \Drupal\file\FileStorage
   */
  protected $fileStorage;

  /**
   * The list of all existing node types.
   *
   * @var array
   */
  protected $nodeTypes;

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->fileStorage = $this->entityTypeManager->getStorage('file');
    $this->nodeTypes = $this->entityTypeManager
      ->getStorage('node_type')
      ->loadMultiple();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ggpushcast.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ggpushcast_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Check if the library (needed for notifications pushing) exists.
    $this->checkIfLibraryExists();

    $config = $this->config('ggpushcast.settings');

    $form['settings'] = [
      '#type' => 'vertical_tabs',
    ];

    $this->buildIconAndBadgeTab($form, $config);

    $this->buildContentTypesTabs($form, $config);
    $this->buildNotificationTitleElements($form, $config);
    $this->buildImageSelectionElements($form, $config);

    $this->buildApiKeyTab($form, $config);

    $form['logging_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create log (collect the statistics of sent and failed notifications).'),
      '#default_value' => 1,
    ];

    // If settings was stored, then set selected values.
    if (!$config->isNew()) {
      $this->setDefaultValuesFromConfig($form, $config);
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Check if the 'ggPushcastsw.js' exists in the root directory of the site.
   *
   * The 'ggPushcastsw.js' library is needed for notifications pushing and it
   * must be placed in the root directory of the site. If the library doesn't
   * exists, then inform a user about it and provide the link to download the
   * library.
   */
  private function checkIfLibraryExists() {

    if (file_exists('ggPushcastsw.js') == FALSE) {
      $args = [
        ':url' => 'https://ggpushcast.com/static/js/ggPushcastsw.js',
        '@name' => 'ggPushcastsw.js',
      ];

      $message = $this->t('YOU NEED TO DOWNLOAD THE <a href=":url">@name</a> LIBRARY AND PLACE IT INTO THE ROOT DIRECTORY OF THE SITE.', $args);

      $this->messenger()->addError($message);
    }
  }

  /**
   * {@inheritdoc}
   */
  private function buildIconAndBadgeTab(array &$form, $config) {
    $form['icon_and_badge'] = [
      '#type' => 'details',
      '#title' => $this->t('Icon & badge'),
      '#group' => 'settings',
      '#open' => TRUE,
    ];

    /* ------------ Icon ---------------------------------------- */

    $form['icon_and_badge']['icon_container'] = [
      '#type' => 'details',
      '#title' => $this->t('Where the icon image should be taken from'),
      '#open' => TRUE,
    ];

    $form['icon_and_badge']['icon_container']['icon_source'] = [
      '#type' => 'radios',
      '#options' => [
        'site_logo' => $this->t('From the site logo'),
        'content' => $this->t('From a node'),
        'ggpushcast.com' => $this->t('From ggpushcast.com'),
        'uploaded_image' => $this->t('Uploaded image'),
      ],
      '#default_value' => 'site_logo',
    ];

    // Create the additional container to avoid the issue with #states on the
    // 'managed_file' element.
    // See: https://www.drupal.org/project/drupal/issues/2847425
    $form['icon_and_badge']['icon_container']['icon_file_container'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[name="icon_source"]' => ['value' => 'uploaded_image'],
        ],
      ],
    ];

    $form['icon_and_badge']['icon_container']['icon_file_container']['icon_file'] = [
      '#type' => 'managed_file',
      '#title' => '',
      '#upload_location' => 'public://ggpushcast-images',
      '#description' => $this->t('Upload the icon image, the recommended size: 192x192 pixels.'),
      '#upload_validators' => [
        'file_validate_extensions' => ['png gif jpg jpeg'],
        'file_validate_size' => [25600000],
        'file_validate_image_resolution' => ['192x192', '192x192'],
      ],
    ];

    /* ------------ Badge ---------------------------------------- */

    $form['icon_and_badge']['badge_container'] = [
      '#type' => 'details',
      '#title' => $this->t('Where the badge image should be taken from'),
      '#open' => TRUE,
    ];

    $form['icon_and_badge']['badge_container']['badge_source'] = [
      '#type' => 'radios',
      '#options' => [
        'favicon' => $this->t('From favicon.ico'),
        'uploaded_image' => $this->t('Uploaded image'),
      ],
      '#default_value' => 'favicon',
    ];

    // Create the additional container to avoid the issue with #states on the
    // 'managed_file' element.
    // See: https://www.drupal.org/project/drupal/issues/2847425
    $form['icon_and_badge']['badge_container']['badge_file_container'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[name="badge_source"]' => ['value' => 'uploaded_image'],
        ],
      ],
    ];

    $form['icon_and_badge']['badge_container']['badge_file_container']['badge_file'] = [
      '#type' => 'managed_file',
      '#title' => '',
      '#upload_location' => 'public://ggpushcast-images',
      '#description' => $this->t('Upload the badge image, the recommended size: 72x72 pixels.'),
      '#upload_validators' => [
        'file_validate_extensions' => ['png gif jpg jpeg'],
        'file_validate_size' => [25600000],
        'file_validate_image_resolution' => ['72x72', '72x72'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  private function buildContentTypesTabs(array &$form, $config) {
    $form['node_types_container'] = [
      '#type' => 'details',
      '#title' => $this->t('Content types'),
      '#group' => 'settings',
    ];

    $form['node_types_container']['node_types_tabs'] = [
      '#type' => 'vertical_tabs',
    ];

    foreach ($this->nodeTypes as $node_type) {
      $type = $node_type->id();
      $type_label = $node_type->label();

      $form[$type] = [
        '#type' => 'details',
        '#title' => $type_label,
        '#group' => 'node_types_tabs',
      ];

      $form[$type][$type . '_enable_notifications'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable push notifications for @type', ['@type' => $type_label]),
      ];

      $form[$type][$type . '_always_notify'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Always send notifications when nodes are publishing'),
        '#states' => [
          'visible' => [
            ':input[name="' . $type . '_enable_notifications"]' => ['checked' => TRUE],
          ],
        ],
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  private function buildNotificationTitleElements(array &$form, $config) {
    foreach ($this->nodeTypes as $node_type) {
      $type = $node_type->id();

      // Get all taxonomy fields of a content type.
      $field_definitions = $this->entityFieldManager->getFieldDefinitions('node', $type);
      $taxonomy_fields = [];

      foreach ($field_definitions as $field_name => $field_definition) {
        if ($field_definition->getType() == 'entity_reference') {
          if ($field_definition->getSetting('target_type') == 'taxonomy_term') {
            $taxonomy_fields[$field_name] = $field_definition->getLabel();
          }
        }
      }

      $form[$type]['title_container'] = [
        '#type' => 'details',
        '#title' => $this->t('The notification title pattern'),
        '#open' => TRUE,
        '#states' => [
          'visible' => [
            ':input[name="' . $type . '_enable_notifications"]' => ['checked' => TRUE],
          ],
        ],
      ];
      // If a content type hasn't taxonomy fields, then make 'radios' disabled.
      $form[$type]['title_container'][$type . '_title_pattern'] = [
        '#type' => 'radios',
        '#disabled' => empty($taxonomy_fields),
        '#options' => [
          'site_name_only' => $this->t('Site name only'),
          'site_name_and_taxonomy' => $this->t('Site name + taxonomy'),
          'taxonomy_and_site_name' => $this->t('Taxonomy + site name'),
        ],
        '#default_value' => 'site_name_only',
      ];

      $form[$type]['title_container'][$type . '_taxonomy_fields'] = [
        '#type' => 'select',
        '#title' => $this->t('Select a taxonomy field'),
        '#options' => $taxonomy_fields,
        '#states' => [
          'invisible' => [
            ':input[name="' . $type . '_title_pattern"]' => ['value' => 'site_name_only'],
          ],
        ],
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  private function buildImageSelectionElements(array &$form, $config) {
    foreach ($this->nodeTypes as $node_type) {
      $type = $node_type->id();

      // Get all image fields of a content type.
      $field_definitions = $this->entityFieldManager->getFieldDefinitions('node', $type);
      $image_fields = [];
      foreach ($field_definitions as $field_name => $field_definition) {
        if ($field_definition->getType() == 'image') {
          $image_fields[$field_name] = $field_definition->getLabel();
        }
      }

      $form[$type]['image_container'] = [
        '#type' => 'details',
        '#title' => $this->t('Where an image should be taken from'),
        '#open' => TRUE,
        '#states' => [
          'visible' => [
            ':input[name="' . $type . '_enable_notifications"]' => ['checked' => TRUE],
          ],
        ],
      ];
      // If a content type hasn't image fields, then make the 'radios' disabled.
      $form[$type]['image_container'][$type . '_image_source'] = [
        '#type' => 'radios',
        '#disabled' => empty($image_fields),
        '#options' => [
          'body' => $this->t('From a body'),
          'field' => $this->t('From a field'),
        ],
        '#default_value' => 'body',
      ];

      $form[$type]['image_container'][$type . '_image_fields'] = [
        '#type' => 'select',
        '#title' => $this->t('Select an image field'),
        '#options' => $image_fields,
        '#states' => [
          'visible' => [
            ':input[name="' . $type . '_image_source"]' => ['value' => 'field'],
          ],
        ],
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  private function buildApiKeyTab(array &$form, $config) {
    $form['api_key'] = [
      '#type' => 'details',
      '#title' => $this->t('API Key'),
      '#description' => '',
      '#open' => TRUE,
      '#group' => 'settings',
    ];

    $form['api_key']['api_key'] = [
      '#type' => 'textfield',
      '#title' => '',
      '#description' => $this->t('Enter API Key'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  private function setDefaultValuesFromConfig(array &$form, $config) {

    /* ------------ Icon --------------------------------------------------- */

    $form['icon_and_badge']['icon_container']['icon_source']['#default_value'] = $config->get('icon_source');

    $form['icon_and_badge']['icon_container']['icon_file_container']['icon_file']['#default_value'] = [
      $config->get('icon_file'),
    ];

    /* ------------ Badge -------------------------------------------------- */

    $form['icon_and_badge']['badge_container']['badge_source']['#default_value'] = $config->get('badge_source');

    $form['icon_and_badge']['badge_container']['badge_file_container']['badge_file']['#default_value'] = [
      $config->get('badge_file'),
    ];

    /* ------------ Content types ------------------------------------------ */

    foreach ($this->nodeTypes as $node_type) {
      $type = $node_type->id();

      $config_node_types = $config->get('node_types');
      // Check if a node type (stored in the config) still exists.
      if (!array_key_exists($type, $config_node_types)) {
        continue;
      }

      $form[$type][$type . '_enable_notifications']['#default_value'] = TRUE;
      if ($config_node_types[$type]['always_notify']) {
        $form[$type][$type . '_always_notify']['#default_value'] = TRUE;
      }

      $image_source = $config_node_types[$type]['image_source'];
      $form[$type]['image_container'][$type . '_image_source']['#default_value'] = $image_source;
      if ($image_source == 'field') {
        $image_field = $config_node_types[$type]['image_field'];
        $form[$type]['image_container'][$type . '_image_fields']['#default_value'] = $image_field;
      }

      $title_pattern = $config_node_types[$type]['title_pattern'];
      $form[$type]['title_container'][$type . '_title_pattern']['#default_value'] = $title_pattern;
      if ($title_pattern != 'site_name_only') {
        $taxonomy_field = $config_node_types[$type]['taxonomy_field'];
        $form[$type]['title_container'][$type . '_taxonomy_fields']['#default_value'] = $taxonomy_field;
      }
    }

    /* ------------ API Key ------------------------------------------------ */

    $form['api_key']['api_key']['#default_value'] = $config->get('api_key');

    /* ------------ Log ---------------------------------------------------- */

    $form['logging_enabled']['#default_value'] = $config->get('logging_enabled');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('ggpushcast.settings');
    $node_types_config = [];

    foreach ($this->nodeTypes as $node_type) {
      $type = $node_type->id();

      if ($form_state->getValue($type . '_enable_notifications') == 1) {
        $node_types_config[$type] = [
          'always_notify' => $form_state->getValue($type . '_always_notify'),
          'image_source' => $form_state->getValue($type . '_image_source'),
          'title_pattern' => $form_state->getValue($type . '_title_pattern'),
        ];
        if ($form_state->getValue($type . '_image_source') == 'field') {
          $node_types_config[$type]['image_field'] = $form_state->getValue($type . '_image_fields');
        }
        if ($form_state->getValue($type . '_title_pattern') != 'site_name_only') {
          $node_types_config[$type]['taxonomy_field'] = $form_state->getValue($type . '_taxonomy_fields');
        }
      }
    }

    // Store the uploaded Icon file.
    $fid = $form_state->getValue(['icon_file', 0]);
    if (!empty($fid)) {
      $file = $this->fileStorage->load($fid);
      $file->setPermanent();
      $file->save();
      $config->set('icon_file', $file->id());
    }
    else {
      $config->set('icon_file', '');
    }

    // Store the uploaded Badge file.
    $fid = $form_state->getValue(['badge_file', 0]);
    if (!empty($fid)) {
      $file = $this->fileStorage->load($fid);
      $file->setPermanent();
      $file->save();
      $config->set('badge_file', $file->id());
    }
    else {
      $config->set('badge_file', '');
    }

    $config->set('node_types', $node_types_config);
    $config->set('icon_source', $form_state->getValue('icon_source'));
    $config->set('badge_source', $form_state->getValue('badge_source'));
    $config->set('api_key', $form_state->getValue('api_key'));
    $config->set('logging_enabled', $form_state->getValue('logging_enabled'));
    $config->save();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->checkSvg($form_state);

    if (empty($form_state->getValue('api_key'))) {
      $args = [
        ':url' => 'https://ggpushcast.com/',
        '@name' => 'ggpushcast.com',
      ];
      $message = $this->t('The API key is needed. You can get it on the <a href=":url">@name</a>.', $args);
      $form_state->setErrorByName('api_key', $message);
    }

    if ($form_state->getValue('icon_source') == 'uploaded_image') {
      $fid = $form_state->getValue(['icon_file', 0]);

      if (empty($fid)) {
        $form_state->setErrorByName('icon_file', $this->t('Please upload the icon image file.'));
      }
    }

    if ($form_state->getValue('badge_source') == 'uploaded_image') {
      $fid = $form_state->getValue(['badge_file', 0]);

      if (empty($fid)) {
        $form_state->setErrorByName('badge_file', $this->t('Please upload the badge image file.'));
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * If an icon is in SVG format then notify a user about the issue in Chrome.
   *
   * When the module was created the Chrome doesn't display notifications icons
   * if they are in SVG format.
   * See: https://bugs.chromium.org/p/chromium/issues/detail?id=478654
   * Therefore, notify a user about that.
   */
  private function checkSvg(FormStateInterface $form_state) {
    if ($form_state->getValue('icon_source') != 'site_logo') {
      return;
    }

    /* ---- Check the extension of an icon file of a front theme ----- */

    // Get the name of a front theme (non admin theme).
    $front_theme_name = $this->config('system.theme')->get('default');
    $logo_url = theme_get_setting('logo.url', $front_theme_name);
    $path_parts = pathinfo($logo_url);
    $icon_extension = $path_parts['extension'];

    if ($icon_extension == 'svg') {
      $args = [
        ':url' => 'https://bugs.chromium.org/p/chromium/issues/detail?id=478654',
        '@name' => "Issue 478654: Can't use SVG for notification icon.",
      ];

      $message = $this->t('The icon is in the SVG format and may not be displayed in Chromium browsers. <br> See: <a href=":url">@name</a>', $args);

      $this->messenger()->addWarning($message);
    }
  }

}
