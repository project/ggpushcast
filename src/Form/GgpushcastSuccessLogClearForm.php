<?php

namespace Drupal\ggpushcast\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Form\ConfirmFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Confirmation form before deleting the log of successfully sent notifications.
 *
 * @internal
 */
class GgpushcastSuccessLogClearForm extends ConfirmFormBase {

  /**
   * State service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->state = $container->get('state');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ggpushcast_success_log_clear_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the log?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('ggpushcast.success_log');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $log_messages_count = count($this->state->get('ggpushcast_success_logs'));

    if ($log_messages_count > 0) {

      $this->state->set('ggpushcast_success_logs', []);
      $this->messenger()->addStatus($this->t('The log cleared (@count entries).', ['@count' => $log_messages_count]));
    }
    else {
      $this->messenger()->addStatus($this->t('The log is already cleared.'));
    }

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
