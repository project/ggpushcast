<?php

namespace Drupal\ggpushcast\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;

/**
 * The log of failed notifications.
 */
class GgpushcastFailedLogForm extends FormBase {

  /**
   * State service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->state = $container->get('state');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ggpushcast_failed_log_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $failed_logs = $this->state->get('ggpushcast_failed_logs');
    $total = count($failed_logs);

    $form['log'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Total: @total', ['@total' => $total]),
        $this->t('Status'),
        $this->t('Title'),
      ],
    ];

    if ($this->userIsAllowedRepushFailedNotification()) {
      // Add an empty header to make the table look nicer.
      $form['log']['#header'][] = '';
    }

    foreach ($failed_logs as $nid => $value) {

      $form['log'][$nid]['date'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => date('d M Y H:i:s', $value['date']),
      ];

      $form['log'][$nid]['status_code'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => $value['status_code'],
      ];

      $node = $this->entityTypeManager->getStorage('node')->load($nid);

      $form['log'][$nid]['title'] = [
        '#type' => 'link',
        // Displaying the current title as it may have changed.
        '#title' => $node->getTitle(),
        '#attributes' => ['target' => '_blank'],
        '#url' => $node->toUrl('canonical'),
      ];

      if ($this->userIsAllowedRepushFailedNotification()) {

        $form['log'][$nid]['push_link'] = [
          '#type' => 'link',
          '#title' => $this->t('Push'),
          '#attributes' => [
            'class' => ['button'],
          ],
          '#url' => Url::fromRoute('ggpushcast.repush_failed_notification', ['node' => $nid]),
        ];
      }
    }

    return $form;
  }

  /**
   * Check if a user allowed to resend failed notifications.
   */
  private function userIsAllowedRepushFailedNotification() {

    $current_user_roles = $this->currentUser()->getRoles();
    $is_admin = in_array('administrator', $current_user_roles);
    $has_resend_permission = $this->currentUser()->hasPermission('resend failed notifications');

    return ($is_admin || $has_resend_permission);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
