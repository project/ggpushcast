<?php

namespace Drupal\ggpushcast\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;

/**
 * The log of successfull notifications.
 */
class GgpushcastSuccessLogForm extends FormBase {

  /**
   * Drupal\Core\State\StateInterface definition.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->state = $container->get('state');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ggpushcast_success_log_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $success_logs = $this->state->get('ggpushcast_success_logs');

    $total = count($success_logs);

    $form['log'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Total: @total', ['@total' => $total]),
        $this->t('Title'),
      ],
    ];

    foreach ($success_logs as $nid => $value) {

      $form['log'][$nid]['date'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => date('d M Y H:i:s', $value['date']),
      ];

      $form['log'][$nid]['title'] = [
        '#type' => 'link',
        // Displaying the title that was at the time of sending.
        '#title' => $value['title'],
        '#attributes' => ['target' => '_blank'],
        '#url' => Url::fromUserInput('/node/' . $nid),
      ];

    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
