<?php

namespace Drupal\ggpushcast;

use Drupal\node\NodeInterface;

/**
 * Defines an interface for ggpushcast notifications classes.
 */
interface GgpushcastNotificationsManagerInterface {

  /**
   * Send a notification data to the 'G&G Pushcast' service.
   *
   * @param \Drupal\node\NodeInterface $node
   *   A node that was published and for which we need to send a notification.
   *
   * @return int
   *   The code of a response:
   *   200 - SUCCESS;
   *   400 - MESSAGE NOT FOUND OR WRONG;
   *   401 - AUTHENTICATION_FAILURE.
   */
  public function push(NodeInterface $node);

  /**
   * Resend a notification data to the 'G&G Pushcast' service.
   *
   * The method is called when a user try to resend a data that wasn't sent due
   * to a failure.
   * To do this a user click on the 'Push' button in the log message table.
   * Also, this method could be called from the queue worker when Cron runs.
   *
   * @param \Drupal\node\NodeInterface $node
   *   A node that was published and for which we need to send a notification.
   *
   * @return int
   *   The code of a response:
   *   200 - SUCCESS;
   *   400 - MESSAGE NOT FOUND OR WRONG;
   *   401 - AUTHENTICATION_FAILURE.
   */
  public function repush(NodeInterface $node);

}
