<?php

namespace Drupal\ggpushcast\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\node\NodeInterface;

/**
 * Process of manually pushing of failed notifications.
 */
class GgpushcastRepushFailedNotificationController extends ControllerBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\ggpushcast\GgpushcastNotificationsManagerInterface definition.
   *
   * @var \Drupal\ggpushcast\GgpushcastNotificationsManagerInterface
   */
  protected $notificationsManager;

  /**
   * State service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->configFactory = $container->get('config.factory');
    $instance->connection = $container->get('database');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->notificationsManager = $container->get('ggpushcast.notifications_manager');
    $instance->state = $container->get('state');
    return $instance;
  }

  /**
   * Repush.
   *
   * @return string
   *   Return Hello string.
   */
  public function repush(NodeInterface $node) {

    if (is_null($node)) {
      $args = ['@nid' => $nid];
      $message = $this->t('Impossible to push the notification because the node @nid no longer exists.', $args);
      $this->messenger()->addWarning($message);
      return $this->redirect('ggpushcast.failed_log');
    }

    if ($node->isPublished() == FALSE) {
      $args = ['@nid' => $nid];
      $message = $this->t('Impossible to push the notification because the node @nid is no longer published.', $args);
      $this->messenger()->addWarning($message);
      return $this->redirect('ggpushcast.failed_log');
    }

    $config = $this->configFactory->getEditable('ggpushcast.settings');
    // Node types that was enabled for notification sending on settings page.
    $types_enabled = $config->get('node_types');
    $type = $node->getType();
    // Check if notification sending is still enabled for this node type.
    if (empty($types_enabled) or !array_key_exists($type, $types_enabled)) {
      $args = ['@type' => ucfirst($type)];
      $message = $this->t('The notification sending is now disabled for the @type content type.', $args);
      $this->messenger()->addWarning($message);
      return $this->redirect('ggpushcast.failed_log');
    }

    // Send a data to to the service.
    $status_code = $this->notificationsManager->repush($node);

    // If a data was sent successfully.
    if ($status_code == 200) {

      $nid = $node->id();

      $cron_queue_items = $this->state->get('ggpushcast_cron_queue_items');
      // Remove a data from the Cron queue to prevent repeating of resending on
      // Cron runs.
      // If a repushing attempt was made when Cron runs, then element will be
      // deleted. Therefore, we need to ensure that the index still exists.
      if (isset($cron_queue_items[$nid])) {
        $queue_item_id = $cron_queue_items[$nid];

        $this->connection->delete('queue')
          ->condition('item_id', $queue_item_id)
          ->execute();

        // Remove this item from the configuration.
        unset($cron_queue_items[$nid]);
        $this->state->set('ggpushcast_cron_queue_items', $cron_queue_items);
      }

      $message = $this->t('The notification was pushed.');
      $this->messenger()->addStatus($message);
    }
    else {
      // If sending failed, then output a status code of a response.
      $args = ['@status_code' => $status_code];
      $message = $this->t('Pushing failed. The response code: @status_code', $args);
      $this->messenger()->addWarning($message);
    }

    return $this->redirect('ggpushcast.failed_log');
  }

}
